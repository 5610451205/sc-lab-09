package Tree_Traversal;

import java.util.ArrayList;

public class PreOrderTraversal implements Traversal {

	@Override
	public ArrayList<Node> traverse(Node node) {
		ArrayList<Node> preOrder = new ArrayList<Node>();
		if(node == null){ return null;}
		preOrder.add(node);
		traverse(node.getLeft());
		traverse(node.getRight()); 
		return preOrder;
	}

}

package Tree_Traversal;

import java.util.ArrayList;

public class InOrderTraversal implements Traversal{

	@Override
	public ArrayList<Node> traverse(Node node) {
		ArrayList<Node> inOrder = new ArrayList<Node>();
		if(node == null){ return null;}
		inOrder.add(node);
		traverse(node.getLeft());
		traverse(node.getRight()); 
		return inOrder;
	}

}

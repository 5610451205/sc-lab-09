package Tree_Traversal;

import java.util.ArrayList;

public class PostOrderTraversal implements Traversal{

	@Override
	public ArrayList<Node> traverse(Node node) {
		ArrayList<Node> postOrder = new ArrayList<Node>();
		if(node == null){ return null;}
		postOrder.add(node);
		traverse(node.getLeft());
		traverse(node.getRight()); 
		return postOrder;
	}

}

package Tree_Traversal;

public class ReportConsole {
	public void display(Node root, Traversal traversal){
		  System.out.println(((PreOrderTraversal)traversal).traverse(root));
		  //System.out.println(((PostOrderTraversal)traversal).traverse(root));
		  //System.out.println(((InOrderTraversal)traversal).traverse(root));
		  //preOrder( root.getLeft() );
		  //preOrder( root.getRight() );
	}
}

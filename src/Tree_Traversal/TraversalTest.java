package Tree_Traversal;

public class TraversalTest {
	public static void main(String[] args){
		ReportConsole report = new ReportConsole();

		Node node = new Node("5", null, null);
		Node node2 = new Node("6", node, null);
		Node node3 = new Node("7", null, node);
		Traversal traversal = new PreOrderTraversal();
		report.display(node,traversal);
	}
}

package model;

import interfaces.Taxable;

import java.util.Comparator;

public class TaxComparator implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {
		double obj1 = ((Taxable)o1).getTax();
		double obj2 = ((Taxable)o2).getTax();
		if(obj1 < obj2){return -1;}
		if(obj1 > obj2){return 1;}
		// TODO Auto-generated method stub
		return 0;
	}



}

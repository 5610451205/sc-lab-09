package model;

import interfaces.Measurable;

public class Country implements Measurable {
	private String name;
	private double population;
	public Country(String name, int population){
		this.name = name;
		this.population = population;
	}
	
	public String getName(){
		return name;
	}
	
	public double getPopulation(){
		return population;
	}
	
	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return this.population;
	}
	
	public String toString(){
		return "";
	}

}

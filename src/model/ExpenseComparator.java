package model;

import java.util.Comparator;

public class ExpenseComparator implements Comparator<Company> {

	@Override
	public int compare(Company o1, Company o2) {
		double b1 = o1.getExpenditure();
		double b2 = o2.getExpenditure();
		if (b1 < b2) return -1;
		if (b1 > b2) return 1;
		// TODO Auto-generated method stub
		return 0;
	}

}

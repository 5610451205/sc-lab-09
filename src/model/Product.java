package model;

import interfaces.Taxable;

public class Product implements Taxable, Comparable {
	private String name;
	private double price;
	public Product(String name, double price){
		this.name = name;
		this.price = price;
	}
	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		return price * 0.07;
	}
	
	public double getPrice(){
		return this.price;
	}
	@Override
	public int compareTo(Object obj) {
		Product other = (Product) obj;
		if(this.price < other.price){
			return -1;
		}
		else if(this.price > other.price){
			return 1;
		}
		return 0;
	}
	
	public String toString(){
		return "Name : "+ name +" Price : "+ price;
	}
	
}

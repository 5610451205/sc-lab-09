package model;

import interfaces.Measurable;
import interfaces.Taxable;

public class Person implements Measurable , Taxable , Comparable {
	private String name;
	private double tall;
	private double annualSalary;
	
	public Person(String name, double tall, double annualSalary){
		this.name = name;
		this.tall = tall;
		this.annualSalary = annualSalary;
	}
	
	public double getAnnualSalary(){
		return this.annualSalary;
	}
	
	public double getMeasure(){
		return this.tall;
	}
	
	//public String getName(){
	//	return this.name;
	//}
	public String toString(){
		return "Name : "+ name +" His height is "+ tall +" His annualSalary is : "+ annualSalary;
	}

	@Override
	public double getTax() {
		double tax = 0;
		double annual = 0;
		if(Math.floor(this.annualSalary) <= 300000){
			tax = this.annualSalary * 0.05;
		}
		else{
			tax = 300000 * 0.05;
			annual = this.annualSalary - 300000;
			tax = tax + (annual * 0.1);
		}
		return tax;
	}

	@Override
	public int compareTo(Object obj) {
		Person other = (Person) obj;
		if(this.annualSalary < other.annualSalary){
			return -1;
		}
		else if(this.annualSalary > other.annualSalary){
			return 1;
		}
		return 0;
	}
}

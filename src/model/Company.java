package model;

import interfaces.Taxable;

public class Company implements Taxable {
	private String name;
	private double revenue;
	private double expenditure;
	private double profit;
	public Company(String name, double revenue, double expenditure){
		this.setName(name);
		this.revenue = revenue;
		this.expenditure = expenditure;
		this.profit = revenue - expenditure;
	}
	@Override
	public double getTax() {
		double tax = 0;
		if (revenue - expenditure >= 0){
			tax = (revenue - expenditure) * 0.3;
		}
		// TODO Auto-generated method stub
		return tax;
	}
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public double getRevenue(){
		return revenue;
	}
	
	public double getExpenditure(){
		return expenditure;
	}
	public double getProfit() {
		return profit;
	}
	
	public String toString(){
		return "Name : " + name + " Revenue : " + revenue + " Expenditure : " + expenditure + " Profit : " + profit;
	}

}

package control;
import java.util.ArrayList;
import java.util.Collections;

import model.Company;
import model.EarningComparator;
import model.ExpenseComparator;
import model.Person;
import model.Product;
import model.ProfitComparator;
import model.TaxComparator;
public class Test {
	public static void main(String[] args){
		Test test = new Test();
		test.testPerson();
		System.out.println("-------------------------");
		test.testProduct();
		System.out.println("-------------------------");
		test.testCompany();
		System.out.println("-------------------------");
		test.testAllTax();
	}
	
	public void testPerson(){
		ArrayList<Person> persons = new ArrayList<Person>();
		persons.add(new Person("A",160,150000));
		persons.add(new Person("B",150,120000));
		persons.add(new Person("C",170,100000));
		
		//Comparable comparator = new Comparable();
		Collections.sort(persons);
		for(Person i: persons){
			System.out.println(i);
		}
		
	}
	
	public void testProduct(){
		ArrayList<Product> products = new ArrayList<Product>();
		products.add(new Product("item1", 300));
		products.add(new Product("item2", 100));
		products.add(new Product("item3", 700));
		
		Collections.sort(products);
		for(Product i: products){
			System.out.println(i);
		}
	}
	
	public void testCompany(){
		ArrayList<Company> companys = new ArrayList<Company>();
		companys.add(new Company("Dell", 1000000, 900000));
		companys.add(new Company("MSII", 2000000, 800000));
		companys.add(new Company("HP", 3000000, 2500000));
		
		Collections.sort(companys, new EarningComparator());
		System.out.println("Revenue sort");
		for(Company i: companys){
			System.out.println(i);
		}
		
		Collections.sort(companys, new ExpenseComparator());
		System.out.println("Expenditure sort");
		for(Company i: companys){
			System.out.println(i);
		}
		
		Collections.sort(companys, new ProfitComparator());
		System.out.println("Profit sort");
		for(Company i: companys){
			System.out.println(i);
		}
	}
	
	public void testAllTax(){
		ArrayList<Object> alls = new ArrayList<Object>();
		Person p1 = new Person("A",160,150000);
		Company c1 = new Company("MSII", 2000000, 800000);
		Product d1 = new Product("item3", 19900);
		System.out.println("Tax A : "+p1.getTax());
		System.out.println("Tax MSII : "+c1.getTax());
		System.out.println("Tax item3 : "+d1.getTax());
		alls.add(p1);
		alls.add(c1);
		alls.add(d1);
		Collections.sort(alls, new TaxComparator());
		System.out.println("Tax sort");
		for(Object i: alls){
			System.out.println(i);
		}
	}
}
